function Draggable(target) {
  var that = this;
  this.el = target;
  this.x = 0;
  this.y = 0;

  this.sx = 0;
  this.sy = 0;
  this.pushed = false;

  this.el.addEventListener('mousedown', function (e) { that.clickdown(e) });
  document.addEventListener('mousemove', function (e) { that.clickmove(e) });
  document.addEventListener('mouseup', function (e) { that.clickup(e) });
}

Draggable.prototype.clickdown = function (e) {
  e.preventDefault();
  var clientX = e.clientX;
  var clientY = e.clientY;
  this.pushed = true;

  // 처음 클릭한 기준좌표 설정
  this.sx = clientX;
  this.sy = clientY;
};

Draggable.prototype.clickmove = function (e) {
  if (!this.pushed) return;
  var clientX = e.clientX;
  var clientY = e.clientY;
  // 이동한 값만큼 현재 좌표에 합산
  this.x = this.x + (clientX - this.sx);
  this.y = this.y + (clientY - this.sy);
  this.move(this.x, this.y);

  // 기준좌표 초기화
  this.sx = clientX;
  this.sy = clientY;
}

Draggable.prototype.clickup = function () {
  this.pushed = false;
}

Draggable.prototype.move = function (x, y) {
  this.el.setAttribute('style', 
  'left: ' + x + 'px;' +
  'top: '  + y +'px;'
  );
}

new Draggable(document.querySelector('.div-1'));
new Draggable(document.querySelector('.div-2'));
new Draggable(document.querySelector('.div-3'));