class Draggable {
  constructor(target) {
    this.el = target;
    this.x = 0;
    this.y = 0;

    this.sx = 0;
    this.sy = 0;
    this.pushed = false;

    this.el.addEventListener('mousedown', this.clickdown.bind(this));
    document.addEventListener('mousemove', this.clickmove.bind(this));
    document.addEventListener('mouseup', this.clickup.bind(this));
  }

  clickdown(e) {
    e.preventDefault();
    const { clientX, clientY } = e;
    this.pushed = true;

    // 처음 클릭한 기준좌표 설정
    this.sx = clientX;
    this.sy = clientY;
  }

  clickmove({ clientX, clientY }) {
    if (!this.pushed) return;

    // 이동한 값만큼 현재 좌표에 합산
    this.x = this.x + (clientX - this.sx);
    this.y = this.y + (clientY - this.sy);
    this.move(this.x, this.y);

    // 기준좌표 초기화
    this.sx = clientX;
    this.sy = clientY;
  }

  clickup() {
    this.pushed = false;
  }

  move(x, y) {
    this.el.setAttribute('style', `
    left: ${ x }px;
    top: ${ y }px;
    `);
  }
}

new Draggable(document.querySelector('.div-1'));
new Draggable(document.querySelector('.div-2'));
new Draggable(document.querySelector('.div-3'));